<?php

function commerce_payu_settings_form($settings = NULL) {
  $form['merchant'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant'),
    '#description' => t('Merchant ID'),
    '#default_value' => $settings['merchant'],
    '#required' => TRUE,
  );

  $form['secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret key'),
    '#description' => t('Secret key'),
    '#default_value' => $settings['secret_key'],
    '#required' => TRUE,
  );

  $form['lu_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Live Update URL'),
    '#description' => t('Live Update URL'),
    '#default_value' => $settings['lu_url'],
    '#required' => TRUE,
  );

  $form['idn_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Instant Delivery Notification URL'),
    '#description' => t('Instant Delivery Notification URL'),
    '#default_value' => $settings['idn_url'],
  );

  $form['irn_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Instant Refund Notification URL'),
    '#description' => t('Instant Refund Notification URL'),
    '#default_value' => $settings['irn_url'],
  );

  $form['debug_mode'] = array(
    '#type' => 'select',
    '#title' => t('Debug mode'),
    '#options' => array(
      '1' => ('On'),
      '0' => ('Off'),
    ),
    '#default_value' => $settings['debug_mode'],
  );

  $form['currency'] = array(
    '#type' => 'select',
    '#title' => t('Currency of merchant'),
    '#options' => array(
      'UAH' => ('Украина'),
      'RUB' => ('Россия'),
    ),
    '#default_value' => $settings['currency'],
    '#required' => TRUE,
  );


  $form['back_ref'] = array(
    '#type' => 'textfield',
    '#title' => t('Back Ref'),
    '#description' => t('Back url for client'),
    '#default_value' => $settings['back_ref'],
  );

  $form['vat'] = array(
    '#type' => 'textfield',
    '#title' => t('VAT'),
    '#description' => t('VAT'),
    '#default_value' => $settings['vat'],
  );

  $form['language'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#description' => t('Language of payment page'),
    '#options' => array(
      "RU" => t('Russian'),
      "EN" => t('English'),
      "RO" => t('Romanian'),
      "DE" => t('German'),
      "IT" => t('Italian'),
    ),
    '#default_value' => $settings['language'],
  );


  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Order description'),
    '#description' => t('Optional description for orders that will displays in PayU Merchant interface. Use placeholder %order for order ID'),
    '#default_value' => $settings['description'],
  );

  return $form;
}

function commerce_payu_payment_transaction_confirm_form($form, &$form_state, $order, $transaction) {
  $formated_amount =  commerce_currency_amount_to_decimal($transaction->amount, $transaction->currency_code);
  $question_params = array('@amount' => $formated_amount,'@corrency'=>$transaction->currency_code,'@order' =>$order->order_id);
  $question = t('Are you sure to confirm @amount @corrency payment amount for order @order',$question_params);
  $path = url('admin/commerce/orders/'.$order->order_id.'/payment',array('absolute'=> TRUE));

  $payment = commerce_payment_method_instance_load($transaction->instance_id);
  $settings = $payment['settings'];

  $form_state += array(
    'idn_data' => commerce_payu_idn($transaction,$settings),
    'idn_url' => $settings['idn_url'],
    'payment_settings' => $settings,
    'payment_path'=> $path,
    'transaction' => $transaction,
  );

  return confirm_form($form, $question, $path);
}

function commerce_payu_payment_transaction_confirm_form_submit($form, &$form_state) {
  $idn_data =  $form_state['idn_data'];
  $transaction = $form_state['transaction'];
  $transaction->status = COMMERCE_CREDIT_PRIOR_AUTH_CAPTURE;
  commerce_payment_transaction_save($transaction);
  $form_state['redirect'] =  $form_state['payment_path'];
  $curl = curl_init();
  $options = array(
    CURLOPT_URL => $form_state['idn_url'],
    CURLOPT_SSL_VERIFYPEER =>FALSE,
    CURLOPT_SSL_VERIFYHOST=>FALSE,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_HEADER => FALSE,
    CURLOPT_FOLLOWLOCATION => TRUE,
    CURLOPT_POST => TRUE,
    CURLOPT_POSTFIELDS => drupal_http_build_query($idn_data),
  );
  curl_setopt_array($curl,$options);
  $result = strip_tags(curl_exec($curl));
  $result_arr = explode('|',$result);
  $hash = array_pop($result_arr);
  $keys = array('ORDER_REF','RESPONSE_CODE','RESPONSE_MSG','IDN_DATE');
  $result_arr = array_combine($keys, $result_arr);
  $sign = _commerce_payu_signature($result_arr, $form_state['payment_settings']['secret_key'],'IDN_IN');
  if ($sign == $hash) {
    $message = t('@message', array('@message' => $result_arr['RESPONSE_MSG']));
    switch ($result_arr['RESPONSE_CODE']) {
      case COMMERCE_PAYU_IDN_STATUS_CONFIRMED:
        $status = 'success';
      break;
      case COMMERCE_PAYU_IDN_STATUS_ALREADY_CONFIRMED:
        $status = 'warning';
      break;
      case COMMERCE_PAYU_IDN_STATUS_MISSING_ORDER_REF:
      case COMMERCE_PAYU_IDN_STATUS_MISSING_AMOUNT:
      case COMMERCE_PAYU_IDN_STATUS_MISSING_CURRENCY:
      case COMMERCE_PAYU_IDN_STATUS_MISSING_DATE_FORMAT:
      case COMMERCE_PAYU_IDN_STATUS_CONFIRMATION_ERROR:
      case COMMERCE_PAYU_IDN_STATUS_UNKNOWN_ERROR:
      case COMMERCE_PAYU_IDN_STATUS_INVALID_ORDER_REF:
      case COMMERCE_PAYU_IDN_STATUS_INVALID_ORDER_AMOUNT:
      case COMMERCE_PAYU_IDN_STATUS_INVALID_ORDER_CURRENCY:
        $status = 'error';
        $transaction->status = COMMERCE_CREDIT_AUTH_ONLY;
      break;
    }
  }
  else {
    $transaction->status = COMMERCE_CREDIT_AUTH_ONLY;
    $message = t('Invalid signature!');
    $status = 'error';
  }
  drupal_set_message($message,$status);
  commerce_payment_transaction_save($transaction);
}

function commerce_payu_payment_transaction_cancel_form($form, &$form_state, $order, $transaction) {
  $formated_amount =  commerce_currency_amount_to_decimal($transaction->amount, $transaction->currency_code);
  $question_params = array('@amount' => $formated_amount,'@corrency'=>$transaction->currency_code,'@order' =>$order->order_id);
  $question = t('Are you sure to cancel @amount @corrency payment amount for order @order',$question_params);
  $path = url('admin/commerce/orders/'.$order->order_id.'/payment',array('absolute'=> TRUE));

  $payment = commerce_payment_method_instance_load($transaction->instance_id);
  $settings = $payment['settings'];

  $form_state += array(
    'irn_data' => commerce_payu_irn($transaction,$settings),
    'irn_url' => $settings['irn_url'],
    'payment_settings' => $settings,
    'payment_path'=> $path,
    'transaction' => $transaction,
  );

  return confirm_form($form, $question, $path);
}

function commerce_payu_payment_transaction_cancel_form_submit($form, &$form_state) {
  $irn_data =  $form_state['irn_data'];
  $form_state['redirect'] =  $form_state['payment_path'];
  $transaction = $form_state['transaction'];
  $transaction->status = COMMERCE_PAYU_IRN_STATUS_SEND;
  commerce_payment_transaction_save($transaction);
  $curl = curl_init();
  $options = array(
    CURLOPT_URL => $form_state['irn_url'],
    CURLOPT_SSL_VERIFYPEER =>FALSE,
    CURLOPT_SSL_VERIFYHOST=>FALSE,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_HEADER => FALSE,
    CURLOPT_FOLLOWLOCATION => TRUE,
    CURLOPT_POST => TRUE,
    CURLOPT_POSTFIELDS => drupal_http_build_query($irn_data),
  );
  curl_setopt_array($curl,$options);
  $result = strip_tags(curl_exec($curl));
  $result_arr = explode('|',$result);
  $hash = array_pop($result_arr);
  $keys = array('ORDER_REF','RESPONSE_CODE','RESPONSE_MSG','IRN_DATE');
  $result_arr = array_combine($keys, $result_arr);
  $sign = _commerce_payu_signature($result_arr, $form_state['payment_settings']['secret_key'],'IRN_IN');
  if ($sign ==$hash) {
    $message = t('@message', array('@message' => $result_arr['RESPONSE_MSG']));
    switch ($result_arr['RESPONSE_CODE']) {
      case COMMERCE_PAYU_IRN_STATUS_CANCELED:
        $status = 'success';
        break;
      case COMMERCE_PAYU_IDN_STATUS_ALREADY_CONFIRMED:
        $status = 'warning';
        break;
      case COMMERCE_PAYU_IRN_STATUS_MISSING_ORDER_REF:
      case COMMERCE_PAYU_IRN_STATUS_MISSING_AMOUNT:
      case COMMERCE_PAYU_IRN_STATUS_MISSING_CURRENCY:
      case COMMERCE_PAYU_IRN_STATUS_MISSING_DATE_FORMAT:
      case COMMERCE_PAYU_IRN_STATUS_CANCELLING_ERROR:
      case COMMERCE_PAYU_IRN_STATUS_UNKNOWN_ERROR:
      case COMMERCE_PAYU_IRN_STATUS_INVALID_ORDER_REF:
      case COMMERCE_PAYU_IRN_STATUS_INVALID_ORDER_AMOUNT:
      case COMMERCE_PAYU_IRN_STATUS_INVALID_ORDER_CURRENCY:
        $status = 'error';
        $transaction->status = COMMERCE_CREDIT_AUTH_ONLY;
      break;
    }
  }
  else {
    $message = t('Invalid signature!');
    $status = 'error';
    $transaction->status = COMMERCE_CREDIT_AUTH_ONLY;
  }
  drupal_set_message($message,$status);
  commerce_payment_transaction_save($transaction);
}